# SISTEMA  WallaTime- CAS DE NEGOCI #

## 1. DESCRIPCIÓ DEL PRODUCTE ##

Avui dia hi ha molta gent que no pot dur a terme activitats a causa d'una escassa capacitat econòmica, aquest projecte vol posar en contacte a aquestes persones que volen oferir un servei a canvi de rebre'n un altre el qual també t'interessa.

## 2. CONTEXT DE NEGOCI ##

El nostre sector de negoci es el sector social.
Volem fer negoci amb la publicitat degut a que la nostra aplicació serà gratuïta i va dirigida a gent privada amb recursos econòmics limitats, de manera que si fos de pagament, no tindria tant ús.
Te una vida, previsiblement llarga, ja que sempre hi haurà gent sense recursos que voldran intercanviar serveis.
 
## 3. OBJECTIUS DEL PRODUCTE ##

1. *Gestionar un intercanvi de serveis entre dos usuaris*. 
2. *Permetre la comunicació entre dos usuaris*. 
3. *Permetre penjar, modificar i eliminar un anunci d’un servei requerit*.
4. *Permetre afegir com a preferit un anunci*.
5. *Permetre afegir un usuari coma  amic*.
6. *Permetre buscar usuaris que sàpiguen fer un determinat servei*.
7. *Permetre buscar serveis necessitats per altres usuaris*.
8. *Permetre validar la identitat de l’usuari per diversos mètodes*.
9. *Permetre visualitzar el perfil d’un altre usuari, les seves habilitats i anuncis*.
10. *Permetre afegir, modificar i eliminar habilitats*.
11. *Permetre modificar la informació personal del perfil*.
12. *Permetre suggerir una nova categoria de servei*.
13. *Permetre reportar a un anunci o usuari inadequat*.
14. *Permetre eliminar la conta i tota la seva informació*.

## 4. RESTRICCIONS ##

1. *Localització*. Aquesta aplicació està destinada només a serveis realitzats a la ciutat Barcelona.
2. *Tecnologia*. Es requereix d'un smartphone o tableta amb localització.
3. *Identitat*. L’usuari s’haurà de registrar amb una identitat real, el seu DNI i mòbil.
4. *Edat*. Només es podran registrar i oferir serveis usuaris majors d’edat.
5. *Internet*. Es requereix de connexió a Internet per connectar-se a l'aplicació.
6. *Serveis*. Per raons legals no es poden oferir tots el tipus de serveis no materials. 
7. *Directe*. Els intercanvis de serveis seran només entre dos usuaris.

## 5. PREVISIÓ FINANCERA ##
En ser una aplicació gratuïta, contem treure benefici a partir dels anuncis. De moment no hem fet un estudi de previsió financera a partir d’aquests, però sí que podem calcular els costos i beneficis estimats.



## 6. RECURSOS ##

[1] http://www.forosdelweb.com/f6/cuanto-cuesta-valor-hora-disenador-grafico-660347/

[2] http://www.mediavida.com/foro/dev/cuanto-cobra-hora-programacion-431601  

