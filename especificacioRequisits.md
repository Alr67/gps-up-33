#SISTEMA  WallaTime - ESPECIFICACIÓ DE REQUISITS DEL SOFTWARE #

## 1. ESPECIFICACIÓ FUNCIONAL ##

### 1.1. Diagrama de casos d'ús

![](http://agilemodeling.com/images/style/useCaseOnlineShopping.gif)


- Cas d´ús UC001 - *Registrar usuari*: Una persona es registra al sistema.

- Cas d’ús UC002 - *Eliminar usuari*: Un usuari es dona de baixa al sistema.

- Cas d’ús UC003 - *Iniciar sessió*: Un usuari pot des-associar la seva conta ja existent a un dispositiu mòbil.

- Cas d’ús UC004 - *Des-associar conta*: Un usuari pot des-associar la seva conta d’un dispositiu mòbil.

- Cas d’ús UC005 - *Verificar conta*: L’usuari pot verificar la conta via diferents mètodes.
	- Cas d’ús UC005.1 - *Verificar conta via Facebook*.
	- Cas d’ús UC005.2 - *Verificar conta via DNI*.
	- Cas d’ús UC005.3 - *Verificar conta via email*.
	- Cas d’ús UC005.4  - *Verificar conta via SMS*.

- Cas d’ús UC006 - *Visualitzar perfil*: Un usuari pot visualitzar el perfil d’un altre usuari.

- Cas d’ús UC007 - *Modificar usuari*: Un usuari pot modificar el seu perfil.
    - Cas d’ús UC007.1 - *Modificar Nom*
    - Cas d’ús UC007.2 - *Modificar Data Naixement*
    - Cas d’ús UC007.3 - *Modificar Residència*: Modifica el lloc on viu.
    - Cas d’ús UC007.4 - *Modifica Sexe*
    - Cas d’ús UC007.5 - *Modificar Contrasenya*
    - Cas d’ús UC007.6 - *Modificar Email*
    - Cas d’ús UC007.7 - *Modificar Habilitats*
    - Cas d’ús UC007.8 - *Modificar Foto Perfil*

- Cas d’ús UC008 - *Afegir habilitat*: Un usuari afegeix una habilitat que podria oferir com a intercanvi.


- Cas d’ús UC009 - *Modificar habilitat*: Un usuari modifica els detalls d’una de les seves habilitats o l’elimina.

- Cas d'ús UC010 - *Buscar servei*: Els usuaris poden buscar que ofereixen o busquen altres usuaris mitjançant un buscador o tags amb filtres.

- Cas d’ús UC011 - *Visualitzar servei*: Un usuari visualitza un servei concret.

- Cas d'ús UC012 - *Oferir servei*: Un usuari podrà crear un anunci nou dient que ofereix o què necessita.

- Cas d’ús UC013 - *Suggerir nova categoria*: L’usuari pot suggerir una nova categoria de servei als administradors.

- Cas d'ús UC014 - *Modificar servei buscat*: L’autor del servei, pot canviar que ofereix i/o que vol a canvi.

- Cas d'ús UC015 - *Eliminar servei buscat*: L’autor d’un servei d’oferta, pot retirar el que a ofert.

- Cas d'ús UC016 - *Reportar servei*: Qualsevol, por reportar un servei perquè no compleix les normes.

- Cas d’ús UC017 - *Afegir preferit*: Un usuari afegeix un servei com a preferit.

- Cas d’ús UC018 - *Compartir anunci*: L’usuari pot enviar un anunci concret a algú altre per diferents mitjans.

- Cas d'ús UC003 - *Proposar intercanvi*: Un cop un usuari trobi que vol, oferirà el seu servei i es posarà en contacte amb l’usuari que ha creat l’anunci de servei.

- Cas d'ús UC004 - *Acceptar intercanvi*: L’usuari accepta una proposta d’intercanvi.

- Cas d’us UC005 - *Rebutjar intercanvi*: L’usuari rebutja  una proposta d’intercanvi.

- Cas d’ús UC020 - *Modificar proposta*: Un usuari pot modificar les dades de la proposta d’intercanvi de serveis rebuda i enviar la nova proposta a l’altre usuari.

- Cas d'ús UC024 - *Cancel·lar Proposta d'intercanvi*: L'usuari pot cancel·lar una proposta d'intercanvi que encara no hagi començat.

- Cas d'ús UC006 - *Valorar usuari*: Quan s’hagi fet la feina, les 2 persones podran votar a l’altre en funció de la feina feta.

- Cas d'ús UC007 - *Reportar usuari*: Qualsevol, por reportar a un altre usuari per conducta inadequada, etc.

- Cas d’ús UC025 - *Obrir xat*: L’usuari pot obrir un xat a un altre usuari.

- Cas d’ús UC027 - *Visualitzar xat*: Un usuari visualitza el xat amb un usuari concret.

- Cas d’ús UC028 - *Eliminar xat*: Un usuari elimina un o més dels xats oberts que té.

- Cas d’ús UC029 - *Crear anunci*: Un usuari afegeix al sistema un anunci.

- Cas d’ús UC030 - *Modificar anunci*: Un usuari modifica un dels seus anuncis

- Cas d’ús UC031 - *Eliminar anunci*: Un usuari elimina un dels seus anuncis.

https://www.draw.io/?#G0B60xbBRSBDsxTGN2RV9XWWRFRHc


### 1.2. Descripció individual dels casos d'ús

#### Cas d'ús UC001 - *Registrar usuari* ####

*Descripció*
Una persona es vol registrar al sistema.

*Flux bàsic*
1. La persona entra a la aplicació
2. L’hi dona la opció de crear un nou usuari
3. Introdueix la informació bàsica com, DNI, Nom d’usuari, contrasenya, etc.

*Pre-condicions*
Aquesta persona no te un usuari en el sistema.

*Post-condicions*
Es crea un nou usuari en el sistema.

#### Cas d'ús UC002 - *Eliminar usuari* ####

*Descripció*
Un usuari vol eliminar la seva conta.

*Flux bàsic*
1. L’usuari selecciona la opció d’eliminar conta.
2. Confirma que vol eliminar la seva conta.

*Pre-condicions*
L’usuari es troba en el seu perfil.

*Post-condicions*
S’elimina de la base de dades l’usuari en qüestió i la seva informació.

#### Cas d'ús UC003 - *Iniciar sessió* ####

*Descripció*
Un usuari vol iniciar sessió en un dispositiu.

*Flux bàsic*
1. L’usuari omple les dades de la seva conta ja registrada.
2. S’inicia l’aplicació en el buscador d’anuncis.

*Flux altern 1*
1. Si l’usuari no té cap conta ja existent, passa al cas d’ús *Registrar usuari* 
2. Es retorna al punt del flux bàsic.

*Flux altern 2*
1. Si les dades de la conta són incorrectes, apareix un missatge indicant-ho.
2. Es tornen a omplir les dades en el punt 1 del flux bàsic.

*Pre-condicions*
No hi ha cap conta assignada en aquell dispositiu.

*Post-condicions*
L’aplicació d’aquell dispositiu s’associa a la conta d’usuari proporcionada.

#### Cas d'ús UC004 - *Des-associar conta* ####

*Descripció*
Un usuari vol des-associar la seva conta d’usuari d’un dispositiu en concret.

*Flux bàsic*
1. L’usuari selecciona la opció de des-associar conta.
2. L’usuari confirma que vol associar la seva conta del dispositiu actual.

*Pre-condicions*
L’usuari es troba en el seu perfil.

*Post-condicions*
Es des-associa la conta actual del dispositiu en el que es troba.

#### Cas d'ús UC005 - *Verificar conta* ####

*Descripció*
L’usuari vol verificar el seu compte amb diferents sistemes.

*Flux bàsic*
1.L’usuari selecciona amb el sistema que vol verificar el seu compte.
2.L’usuari fa Login amb el sistema que vol verificar el compte.
3.L’usuari accepta connectar el compte.

*Pre-condicions*
L’ usuari te un compte en el sistema amb el que vol verificar el seu compte.

*Post-condicions*
El sistema connecta els dos comptes.

#### Cas d'ús UC018 - *Visualitzar perfil* ####

*Descripció*
Es mostra el perfil de l’usuari seleccionat.

*Flux bàsic*
1. Es selecciona un usuari.
2. El sistema mostra el perfil del usuari seleccionat.

*Flux altern*
1. Es pot modificar el perfil si es el perfil propi.

*Pre-condicions*
L’ usuari que es vol visualitzar el perfil existeix.

*Post-condicions*
Es mostra el perfil del usuari.

#### Cas d'ús UC007 - *Modificar usuari* ####

*Descripció*
L’usuari modifica algun dels detalls del seu perfil: nom, data de naixement, adreça de residència, sexe, contrasenya, email o foto de perfil.

*Fluxe bàsic*
1. L’usuari selecciona l’atribut que vol modificar.
2. El modifica amb la nova informació.

*Post-condicions*
S’actualitza la informació personal de l’usuari.

#### Cas d'ús UC008 - *Afegir habilitat* ####

*Descripció*
L’usuari vol afegir una nova habilitat en el seu perfil.

*Fluxe bàsic*
1. L’usuari selecciona de quina categoria és l’habilitat que vol afegir.
2. L’usuari posa un nom a l’habilitat o servei que ofereix.
3. L’usuari omple la descripció de l’habilitat amb els detalls escaients.

*Pre-condicions*
L’usuari no té cap habilitat amb el mateix nom que la nova.

*Post-condicions*
Apareix una nova habilitat en el perfil de l’usuari.

#### Cas d'ús UC009 - *Modificar habilitat* ####

*Descripció*
L’usuari vol modificar els detalls d’alguna de les seves habilitats.

*Flux bàsic*
1. L’usuari selecciona l’habilitat que vol modificar.
2. L’usuari modifica les dades de l’habilitat que li interessin.

*Flux altern*
1. L’usuari pot eliminar l’habilitat.

*Pre-condicions*
L’usuari es troba en el seu perfil i té alguna habilitat.

*Post-condicions*
Els detalls de l’habilitat en qüestió han estat modificats.

#### Cas d'ús UC010 - *Buscar servei* ####

*Descripció*
L’usuari busca serveis que compleixin els filtres de la última cerca o els predefinits.

*Flux bàsic*
1. S’obre el buscador de serveis
2. Apareixen serveis a partir dels filtres predefinits o la última cerca de l’usuari.

*Flux altern*
1. L’usuari selecciona el botó de la lupa per a definir uns altres filtres de cerca.
2. Apareixen els serveis que compleixen els nous filtres.

*Post-condicions*
Apareixen els serveis que compleixen els filtres seleccionats.

#### Cas d'ús UC011 - *Visualitzar servei* ####

*Descripció*
Un usuari vol veure els detalls d’un determinat servei.

*Flux bàsic*
1.L’usuari selecciona el servei que vol visualitzar amb més profunditat.
2.S’obre el perfil del servei amb tots els detalls que li ha posat el seu anunciant.

*Pre-condicions*
El servei que es vol visualitzar existeix.

#### Cas d'ús UC012 - *Oferir servei* ####

*Descripció*
L’usuari crea un anunci d’un servei que necessita.

*Flux bàsic*
1. L’usuari clica el botó de crear un nou anunci.
2. L’usuari selecciona de quina categoria és el servei que necessita.
3. L’usuari posa un nom al servei que necessita.
4. L’usuari selecciona la localització en la que necessita el servei.
5. L’usuari afegeix una descripció per ampliar la informació del servei necessari.

*Post-condicions*
Hi ha un nou servei en el sistema i en el perfil de l’usuari.

#### Cas d'ús UC013 - *Suggerir nova categoria* ####

*Descripció*
Un usuari envia als administradors una proposta per una nova categoria.

*Flux bàsic*
1. L’usuari accedeix al menú de la aplicació i selecciona la opció d’afegir una nova categoria.
2. L’usuari completa els camps necessaris com, nom de la categoria, descripció, etc.

*Pre-condicions*
La categoria no existeix al sistema.

*Post-condicions*
La categoria s'introdueix al sistema un cop ha estat revisada i acceptada pels administradors.

#### Cas d'ús UC014 - *Modificar servei buscat* ####

*Descripció*
L’usuari vol modificar un anunci seu.

*Flux bàsic*
1. L’usuari selecciona l’anunci que vol modificar.
2. L’usuari selecciona l’opció de modificar anunci.
3. L’usuari canvia el que vol modificar.
4. L’usuari prem el botó de desar els canvis.

*Pre-condicions*
L’usuari es l’autor del servei.

*Post-condicions*
Algun paràmetre del servei es modifica.

#### Cas d'ús UC015 - *Eliminar servei buscat* ####

*Descripció*
L’usuari que ha penjat un anunci d’un servei que necessita el vol eliminar.

*Flux bàsic*
1. L’usuari escull l’anunci que vol eliminar.
2. Li dona a l’opció d’eliminar.

*Pre-condicions*
L’usuari es troba en el seu perfil i té anuncis de serveis que necessita.

*Post-condicions*
L’anunci del servei buscat s’elimina del sistema.

#### Cas d'ús UC016 - *Reportar servei* ####

*Descripció*
Un usuari vol reportar un servei perquè no compleix les normes.

*Flux bàsic*
1. L’usuari selecciona el motiu del report.
2. L’usuari afegeix més informació del motiu.

*Pre-condicions*
L’usuari es troba en el servei que vol reportar.

*Post-condicions*
S’envia l’informe als Administradors.

#### Cas d'ús UC017 - *Afegir preferit* ####

*Descripció*
L’usuari vol afegir com a preferit un anunci o habilitat d’un usuari.

*Flux bàsic*
1. L’usuari va al perfil d’un altre usuari o selecciona un anunci.
2. L’usuari afegeix com a preferit l’usuari o l’anunci seleccionat .

*Pre-condicions*
Ha d’existir l’ usuari o anunci que es vol afegir a preferits.

*Post-condicions*
S’afegeix l’ usuari o l’ anunci seccionat a preferits.

#### Cas d'ús UC18 - *Compartir anunci* ####

*Descripció*
L’usuari envia el link de l’anunci que vol compartir per el sistema que seleccioni.

*Flux bàsic*
1.L’usuari selecciona l’anunci que vol compartir.
2.L’usuari selecciona el sistema mitjançant el que vol compartir l’anunci.

*Pre-condicions*
L’anunci existeix.

*Post-condicions*
Es compateix el link de l’anunci.

#### Cas d'ús UC019 - *Proposar intercanvi* ####

*Descripció*
Un usuari proposa un intercanvi de serveis a un altre usuari.

*Flux bàsic*
1. L’usuari clica al botó de proposar intercanvi.
2. Selecciona una de les seves activitats per a fer l’intercanvi.
3. Omple la informació de l’intercanvi que li aniria bé.

*Flux altern*
1. En cas de no tenir assignada l’habilitat que vol oferir a canvi, pot anar a al cas d’ús Afegir habilitat.
2. Un cop afegida la nova habilitat, se selecciona aquesta per a l’intercanvi.
3. Es retorna al punt 3 del flux bàsic.

*Pre-condicions*
L’usuari que vol proposar el canvi està en un servei buscat per l’altre usuari.

*Post-condicions*
L’altre usuari rep la proposta d’intercanvi de serveis.

#### Cas d'ús UC020 - *Acceptar intercanvi* ####

*Descripció*
Un usuari que ha rebut una proposta d’intercanvi entre dos serveis i hi està d’acord, l’accepta.

*Flux bàsic*
1. Selecciona la proposta que li interessa i que vol.
2. Selecciona a “Acceptar proposta”.
3. S’assumeix que es realitzarà l’intercanvi de serveis proposat a partir de les dades de la proposta.

*Pre-condicions*
L’usuari té una proposta d’intercanvi de serveis.

*Post-condicions*
L’anunci de servei corresponent a la proposta queda marcat com a programat.

#### Cas d'ús UC021 - *Rebutjar intercanvi* ####

*Descripció*
A un dels dos usuaris no els interessa que ofereix l’altre i no accepten l’intercanvi de temps.

*Flux bàsic*
1. Un usuari selecciona la proposta que l’hi interessa.
2. Els dos usuaris es posen en contacte.
3. A un dels 2 no els hi sembla bé alguna cosa tractada.
4. La proposta es cancel·la.

*Pre-condicions*
S’hagi proposat un intercanvi.

*Post-condicions*
La proposta encara es pot buscar per un altre usuari.

#### Cas d'ús UC022 - *Modificar proposta* ####

*Descripció*
L’usuari modifica algun del paràmetres de la seva oferta de servei.

*Flux bàsic*
1. L’usuari selecciona un dels serveis de la seva llista d’ofertes.
2. L’usuari l’hi dona al botó de modificar.
3. L’usuari fa el canvis que cregui convenient.
4. L’usuari l’hi dona al botó de desar els canvis .
*Pre-condicions*
L’usuari ha de ser qui va fer la proposta.

*Post-condicions*
La proposta queda modificada.

#### Cas d'ús UC023 - *Cancel·lar proposta d’intercanvi* ####

*Descripció*
Un usuari vol cancel·lar una proposta d’intercanvi realitzada o acceptada per ell que encara no ha començat.

*Flux bàsic*
1. L’usuari selecciona la proposta que vol cancel·lar.
2. L’usuari confirma que vol cancel·lar la proposta.

*Pre-condicions*
L’usuari té propostes d’intercanvi de serveis.

*Post-condicions*
Se li notifica a l’altre usuari que la proposta en qüestió s’ha cancel·lat.

#### Cas d'ús UC024 - *Valorar usuari* ####

*Descripció*
Després d’haver realitzat un intercanvi de serveis, l’usuari pot valorar el servei rebut per part de l’altre usuari.

*Flux bàsic*
1. L’usuari selecciona la proposta d’intercanvi dels serveis ja realitzats i selecciona a valorar.
2. L’usuari valora cada un dels atributs de la valoració en funció de la qualitat del servei rebut i el tracte amb l’altre usuari.

*Pre-condicions*
Hi ha hagut un intercanvi de serveis entre l’usuari valorat i l’usuari que vol valorar.

*Post-condicions*
Es modifiquen les valoracions de l’usuari valorat en funció de la nova valoració.

#### Cas d'ús UC025 - *Reportar usuari* ####

*Descripció*
Un usuari vol reportar a un altre usuari per comportament indegut o alguna queixa.

*Flux bàsic*
1. Se selecciona l’usuari que es vol reportar.
2. Es clica a reportar usuari.
3. Se selecciona el motiu de la queixa.
4. L’usuari pot ampliar la informació amb una descripció dels motius.

*Pre-condicions*
Hi hagi hagut algun contacte entre els dos usuaris.

*Post-condicions*
S’envia als administradors l’informe.

#### Cas d'ús UC026 - *Obrir Xat* ####

*Descripció*
Un usuari vol posar-se en contacte amb un altre usuari.

*Flux bàsic*
1. L’usuari interessat selecciona el botó xat de l’anunci o el perfil de l’usuari amb qui es vol posar en contacte.
2. Li escriu el missatge.

*Pre-condicions*
L’usuari interessat es troba al perfil de l’usuari amb el que es vol comunicar o en la visualització d’algun dels seus anuncis.

*Post-condicions*
L’usuari amb el qual es vol posar en contacte rep el missatge.

#### Cas d'ús UC027 - *Visualitzar xat* ####

*Descripció*
L’usuari vol visualitzar un xat que té amb un altre usuari en concret.

*Flux bàsic*
1. L’usuari accedeix al registre de xats del seu perfil.
2. L’usuari selecciona el xat que vol visualitzar.

*Post-condicions*
Es visualitza el xat que l’usuari té amb altes usuaris.

#### Cas d'ús UC028 - *Eliminar xat* ####

*Descripció*
L’usuari vol eliminar un o més dels xats que té oberts amb altres usuaris.

*Flux bàsic*
1. L’usuari selecciona un o més xats dels que té oberts.
2. L’usuari confirma que vol eliminar els xats seleccionats.

*Pre-condicions*
L’usuari té algun xat amb algun altre usuari.

*Post-condicions*
Els xats eliminats s’esborren i ja no apareixen a la llista de xats de l’usuari.


## 2. ESPECIFICACIÓ NO FUNCIONAL ##

1. L’ús de l’aplicació ha de ser senzill i intuïtiu. L’aprenentatge dels usuaris sobre com funciona la interfície i els mètodes de l’aplicació haurà de prendre molt poc temps, preferiblement en qüestió de 5 minuts o ments. Aixi aconseguim una aplicació que no requereix un aprenentatge específic i que pot ser utilitzada per tothom.
2. Els intercanvis realitzats a la nostra aplicació hauran de ser serveis, és a dir, no es poden intercanviar coses materials. Amb això promovem a que els usuaris facin servir el seu temps i habilitats, ja que és el que vol promoure la nostra aplicació.
3. Les persones menors d’edat no podrà fer servir la nostra aplicació. Els menors requereixen de l’autorització dels seus tutors per dur a terme certes accions, ja que estan sota la seva tutela. Per evitar problemes legals, vetem l’ús de la aplicació a menors.
4. L’aplicació mantindrà els dades dels usuaris amb la seguretat que corresponen a aquestes, per tal d’evitar que aquestes puguin ser robades o fetes servir amb una finalitat diferent a la de la nostra aplicació.
5. Tant els servidors de la nostra aplicació com aquesta mateixa farà un ús responsable dels recursos, amb la finalitat de reduir el consum energètic i, per tant, fent que la nostra aplicació sigui amigable amb el medi ambient.
6. L'intercanvi ha de ser entre les mateixes dues persones.

![](http://i.imgur.com/4x6xfzM.png)

![](http://i.imgur.com/Dfa7sTe.png)

![](http://i.imgur.com/1dFQy5l.png)
