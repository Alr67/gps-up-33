# SISTEMA WallaTime - GLOSSARI #

- **Anunci de servei:** Entenem com  anunci d’un servei com un servei que l’usuari anunciat necessita.

- **Habilitat:** habilitat o coneixement d’un usuari sobre un tema en concret que l’usuari pot oferir com a intercanvi per als serveis que ell necessiti.

- **Sanció anunci il•legal:** els usuaris que hagin publicat un anunci il•legal o que en vulguin penjar un, tindran una sanció en funció de la gravetat: ja sigui bloqueig de conta, bloqueig per a penjar nous anuncis o bloquejos temporals. 

- **Perfil d’usuari:** zona personal de cada usuari on es poden visualitzar les dades d’aquests.

- **Global Positioning System (GPS):**  Sistema que permet determinar a tot el mon la posició d’un objecte. 

- **Document Nacional d’Identitat (DNI):** Document de l’estat Espanyol que identifica a un ciutadà.

- **Xat:** Canal de comunicació escrit instantani entre dos usuaris.

- **Reportar:** Informar als administradors del sistema sobre un usuari o anunci inadequat.

- **Validar compte:** Certificar, mitjançant diversos sistemes, que una persona és qui diu ser.

- **Desassociar compte:** Acció que desvincula el compte del dispositiu.

- **Dades mòbils:** Connexió que permet a un mòbil comunicar-se per Internet.

