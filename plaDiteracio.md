
# WallaTime- PLA D'ITERACIÓ DE INCEPTION #

## 1. PRESENTACIÓ DE LA ITERACIÓ ##

En la iteració d’inception definirem la visió de projecte, crearem el cas de negoci. Aquesta iteració tindrà una durada de 2 setmanes que aniran del 15 d’Octubre fins el 30 d’Octubre. 
En aquesta iteració l’enginyer de requisit te la part mes important. El gestor de projectes i l’arquitecte de software faran la planificació del projecte.

## 2. COBERTURA DE CASOS D'ÚS ##

Al final de la iteració d’inception tots els casos d’us estaran identificats. 

## 3. ACTIVITATS ##

- Definir visió del projecte: 4 dies.
- Definir cas de negoci: 4 dies, ha de estar acabada la visió del projecte.
- Determinar cas de projecte: 4 dies, ha de estar acabada la visió del projecte.
- Crear el pla de desenvolupament del software: 6 dies, les altres 3 activitats han de estar acabades.


## 4. DIAGRAMA DE GANTT ##

![](http://i.imgur.com/dRhoUdY.png)
