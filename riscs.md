# SISTEMA WallaTime - RISCS #

## RISC 001. Fracas de l’app ##

### Descripció ###

Tenim moltes probabilitats de no funcionar al mercat per desconeixement, per part del usuaris, o per competidors emergents de grans companyies, facebook, twitter, etc.

### Probabilitat ###

Molt probable.
 
### Impacte ###

Això pot suposar el fracàs total del sistema.

### Indicadors ###

El nombre d’usuaris, ofertes de serveis i el nombre de contractes d’intercanvi.
 
### Estratègies de mitigació ###

Fer publicitat en altres aplicacions, anuncis via diferents medis de comunicació i sobretot a les xarxes socials, per promocionar el seu ús.
 
### Plans de mitigació ###

Fer servir inicialment una versió Beta per sentir el feedback dels usuaris i preveure si tindrà èxit o no. 
Començar amb una versió bàsica amb funcionalitats limitades perquè en cas de fracas, la pèrdua sigui menor.


## RISC 002. Fiabilitat ##

### Descripció ###

Usuaris nous amb poques valoracions.

### Probabilitat ###

Molt probable, tots el usuaris seran nous en algun moment.
 
### Impacte ###

Pot tenir un impacte elevat, ja que el fet de no tenir-ne valoracions, es similar a tenir una valoració negativa i és un motiu de desconfiança. Per tant aquest usuari tindrà problemes en trobar persones que necessitin els seus serveis, i per culpa d’aquest fet, deixar la aplicació.
  
### Indicadors ###

Els nous usuaris no tenen cap valoració, i la gent utilitza aquesta per saber si son de confiança o no.
 
### Estratègies de mitigació ###

Tractar els usuaris nous diferent que els usuaris amb valoració negativa.
 
### Plans de mitigació ###

Tots els usuaris comencen amb una valoració diferent a la dels usuaris mal valorats.


## RISC 003. Beneficis insuficients  ##

### Descripció ###

No hem aconseguit els beneficis suficients per cobrir el cost del projecte.

### Probabilitat ###

Probable, pot passar si la aplicació no te cap èxit o el pla d’anuncis escollit no resulta ser l’adequat.
 
### Impacte ###

Un impacte alt .
  
### Indicadors ###

Els beneficis inicials son menors que el cost del projecte.
 
### Estratègies de mitigació ###

Fer publicitat de l’aplicació en les xarxes socials i intentar reanimar-la.
 
### Plans de mitigació ###
Augmentar la publicitat mentre hi hagi poc ús.



## RISC 004. Fals usuari  ##

### Descripció ###

L’usuari no es el propietari del DNI amb el que s’ ha registrat.

### Probabilitat ###

Poc probable.
 
### Impacte ###

Un impacte baix.
   
### Estratègies de mitigació ###

Comprovació de la validesa de les dades proporcionades per l’usuari.

### Plans de mitigació ###

Comprovar que el DNI sigui correcte i tenir un sistema d’autenticació per DNI electrònic.


## RISC 005. Serveis il·legals  ##

### Descripció ###

El servei que ofereix un usuari es un servei il·legal.

### Probabilitat ###

Probable, pot passar.
 
### Impacte ###

Un impacte alt ja que violaria la llei a través de la nostra aplicació i això podria portar a la gent a desconfiar de nosaltres o a tenir problemes legals.
  
### Estratègies de mitigació ###

Control del serveis oferts a l’aplicació.
 
### Plans de mitigació ###

Filtrar els serveis abans de que els usuaris els puguin oferir.


## RISC 006. Canvi de tecnologia ##

### Descripció ###

La tecnologia per la que s’ha dissenyat l’aplicació passa a estar obsoleta.

### Probabilitat ###

Probable, pot passar degut a que la tecnologia canvia constantment.
 
### Impacte ###

Un impacte alt ja que ens hauríem d’adaptar a la nova tecnologia, amb la nova inversió que això suposa o l’aplicació deixaria de fer servei.
  
### Indicadors ###

Sortida d’una nova tecnologia que deixi obsolet el sistema amb el qual treballem.
 
### Estratègies de mitigació ###

Buscar tecnologia que tingui una vida llarga i fer el sistema fàcil d’adaptar als canvis.
 
### Plans de mitigació ###

Desenvolupa el sistema sobre una tecnologia que tingui encara una vida llarga per endavant i amb un disseny que sigui adaptable als canvis .

## RISC 007. Servei Frau ##

### Descripció ###

El servei que ofereix un usuari no és real o pretén aprofitar-se de la gent.

### Probabilitat ###

Probable.
 
### Impacte ###

Els usuaris hauran realitzat un servei sense rebre res a canvi, fet que provocaria una pèrdua de confiança en el sistema i fins i tot mala publicitat.
 
### Indicadors ###

Queixes per part dels usuaris.
 
### Estratègies de mitigació ###

Usar un sistema de valoracions pels usuaris.
 
### Plans de mitigació ###

Implementar un sistema de valoracions que permeti indicar la qualitat del servei rebut o marcar com a incomplert de manera que els altres usuaris puguin saber si aquell usuari és de fiar o no.



## RISC 008. Interfície poc atractiva##

### Descripció ###

La interfície i ús de l’aplicació no són atractius pels usuaris.

### Probabilitat ###

Versemblant.
 
### Impacte ###

Si l’aplicació és difícil d’utilitzar o desagradable, els usuaris tendiran a fer servir algun competidor.
 
### Indicadors ###

Valoracions dels usuaris.
 
### Estratègies de mitigació ###

Qüestionari durant la beta.

### Plans de mitigació ###

Mesures per reduir l'impacte del risc quan es produeix.

