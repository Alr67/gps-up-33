# SISTEMA WallaTime - VISIÓ #

## 1. INTRODUCCIÓ ##

Avui dia, degut a la crisi, hi ha molta gent que no pot dur a terme activitats a causa d'una escassa capacitat econòmica, posant com a exemple assistir a cursos acadèmics, o contractar un cangur per cuidar als nens. Paradoxalment, però, la gent normalment si que té molt a oferir, ja que cadascú te les seves pròpies habilitats i coneixements. Així, trobem a molta gent que podria fer molt més del que fa, i tot per problemes econòmics.
La nostra app vol solucionar això proposant que les persones intercanviïn el que poden oferir a altres persones per coses reciproques, és a dir, oferir un servei a canvi de rebre'n un altre el qual també t'interessa.
Amb això aconseguim eliminar la barrera econòmica, aprofitem les habilitats i coneixements de cadascú i ajudem a que cada persona pugui sentir-se satisfeta ajudant als demès.

## 2. EL PROBLEMA ##

Actualment hi ha molta gent sense feina ni gaires diners que tenen massa temps lliure i no saben com aprofitar-lo o fer-ne alguna cosa útil, fet que en molts casos porta a una baixada de l'autoestima o fins i tot a depressions. Aquesta gent, també es troba en que té moltes necessitats que no pot satisfer per ella mateixa i no pot contractar a ningú per falta de diners.
Això no només passa amb la gent sense feina, sinó també amb gent que treballa a mitja jornada, amb gent gran que un cop jubilada, se sent inútil i amb ganes de continuar fent coses per la societat o el jovent, que necessita algunes coses però no té diners ni habilitats per a aconseguir-les i no li faria res invertir part del seu temps per aconseguir-ho.
Aquestes necessitats poden abastar serveis de molts àmbits, sempre mesurats a partir del temps que requereixen i poden ser tant de coneixements, habilitats, etc.

## 3. PARTS INTERESSADES ##

1. *Gent amb temps lliure*. Interès per aprofitar el seu temps i ja de pas, ajudar a algú altre i rebre alguna cosa que ell no pot fer a canvi. 
2. *Gent amb recursos limitats*. Obtenir certs serveis sense la necessitat de recursos.
3. *Gent que no vol diners*. A vegades pot sortir més a compte un intercanvi que diners.

## 4. EL PRODUCTE ##

El sistema permet posar en contacte a través d’Internet a dues persones que ofereixen o busquen diferents serveis, ja siguin a llarg termini o d'un sol dia.
Hi haurà un sistema de valoracions i comentaris dels usuaris per a poder saber què et pots esperar del seu servei i la seva qualitat.
Per identificar els usuaris a nivell d'aplicació, es farà servir el DNI i número de mòbil per a que no hi hagi duplicitats de contes i el nom per a interactuar amb els altres usuaris.
Cada anunci s'ubica segons el barri i carrer, mostrant per defecte primer els més pròxims a l'usuari.

### 4.1. Perspectiva del producte ###

El sistema es basa en una app per a mòbils Android i iOS, amb connexió a Internet i localització GPS. La app es connectarà a un servidor que proveirà d'informació sobre les ofertes disponibles, fent servir la localització que el GPS haurà proporcionat.

![](http://i.imgur.com/rdsO6mq.jpg?1)

### 4.2. Descripció del producte ###

1. *Registre*. La app demana a l'usuari les dades personals escaients per funcionar.
2. *Penjar Anunci*. La app permet publicar un anunci a l'usuari sobre el servei que vol oferir.
3. *Realitzar Intercanvi*. Funció per acceptar una oferta a canvi de la teva, si l'altre usuari també està interessat.
4. *Puntuar intercanvi realitzat*. Funció que permet donar una valoració sobre el servei prestat en un intercanvi per part de l’altre usuari.
5. *Buscar Serveis concrets*. Cerca per criteris, paraules, localització, etc per trobar ofertes concretes.
6. *Posar-te en contacte*. Funció que permet que dos usuaris es posin en contacte per ofertes que els pugui interessar mútuament.
7. *Afegir a preferits*. Funció que permet marcar un anunci concret com a preferit, per a poder retornar-hi amb facilitat en un altre moment.
8. *Enviar Anunci*. Funció que permet enviar un anunci concret a un altre usuari. 
9. *Associar Conta*. Funció que permet associar una conta d’usuari ja creada en un dispositiu mòbil.
10. *Des-associar Conta*. Funció que permet des-associar una conta d’usuari d’un dispositiu mòbil.
11. *Afegir habilitats*. Cada usuari té unes habilitats concretes definides per a ell, les quals pot oferir com a intercanvi.


### 4.3. Supòsits de funcionament ###

1. *Dos interessats en canviar una oferta*. Si dos persones tenen habilitats que interessen a ambdós, el sistema els permetrà intercanviar-les.
2. *Persona vol buscar servei*. Si una persona necessita algun servei, ara com classes, podrà fer servir la app per publicar un anunci demanant-ne.
3. *Persona ofereix servei*. Si una persona té alguna habilitat per a oferir, podrà afegir-la al seu perfil per a que els altres usuaris ho puguin veure i trobar.

### 4.4. Dependències sobre altres sistemes ###

1. *Identificador*. Cada usuari necessita el seu DNI com a identificador.
2. *GPS*. La app requerirà fer servir el sistema de GPS, propietari del govern dels EUA.
3. *Internet*. L'aplicació requereix de connexió a Internet per a poder buscar anuncis i posar en contacte els usuaris.
 
### 4.5. Altres requisits ###

1. *Aprenentatge*.  Caldrà que l'app sigui senzilla i intuïtiva, corba d'aprenentatge baixa per a que pugui fer-la servir molta gent.
2. *Bidireccional*. L'intercanvi ha de ser entre les mateixes dues persones.
2. *Immaterial*. No es pot fer cap intercanvi per a coses materials.
3. *Sostenible*. Ha de gastar les mínimes dades mòbils possibles.

## 5. RECURSOS ##

[1] recurs 1
https://sites.google.com/site/alfonsoperezr/investigacion/estructuracin-y-especificacin-de-casos-de-uos

